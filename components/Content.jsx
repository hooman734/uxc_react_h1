export default () => {
    return (
        <>
            <div className="flex items-end justify-center my-32">
                <h1 className="text-6xl text-red-900 ">Create Your Comfort Zone</h1>
            </div>
            <div className="flex items-end justify-center my-44">
                <button className="rounded-2xl bg-gray-100 text-gray-600 py-5 px-10 hover:bg-gray-200 ring-1 ">Shop Now</button>
            </div>
            <div className="grid grid-rows-2 grid-cols-1 gap-1 bg-gray-100 py-3">
                <div className="row-span-1 col-span-1 flex justify-center">
                    <h1 className="text-red-900 text-4xl">High Quality Home Decor</h1>
                </div>
                <p className="text-center text-sm row-span-1 col-span-1 text-gray-600">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
            </div>

        </>

    );
}