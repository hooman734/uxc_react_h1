export default () => {
    return (
      <>
          <nav className="relative flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg mb-3">
              <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
                  <div className="w-full relative flex justify-between lg:w-auto  px-4 lg:static lg:block lg:justify-start">
                      <a className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-no-wrap uppercase text-gray-600" href="#pablo">
                          <span className="text-red-600 ">Your</span> Interior
                      </a>
                      <button className="cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none" type="button">
                          <span className="block relative w-6 h-px rounded-sm bg-gray-600"></span>
                          <span className="block relative w-6 h-px rounded-sm bg-gray-600 mt-1"></span>
                          <span className="block relative w-6 h-px rounded-sm bg-gray-600 mt-1"></span>
                      </button>
                  </div>
                  <div className="lg:flex flex-grow items-center" id="example-navbar-warning">
                      <ul className="flex flex-col lg:flex-row list-none ml-auto">
                          <li className="nav-item">
                              <a className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-gray-600 hover:opacity-75" href="#pablo">
                                  Furniture
                              </a>
                          </li>
                          <li className="nav-item">
                              <a className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-gray-600 hover:opacity-75" href="#pablo">
                                  Lighting
                              </a>
                          </li>
                          <li className="nav-item">
                              <a className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-gray-600 hover:opacity-75" href="#pablo">
                                  Decor
                              </a>
                          </li>
                          <li className="nav-item">
                              <a className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-gray-600 hover:opacity-75" href="#pablo">
                                  Tableware
                              </a>
                          </li>
                          <li className="nav-item">
                              <a className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-gray-600 hover:opacity-75" href="#pablo">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M10 19.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5zm3.5-1.5c-.828 0-1.5.671-1.5 1.5s.672 1.5 1.5 1.5 1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm1.336-5l1.977-7h-16.813l2.938 7h11.898zm4.969-10l-3.432 12h-12.597l.839 2h13.239l3.474-12h1.929l.743-2h-4.195z"/></svg>
                              </a>
                          </li>
                          <li className="nav-item">
                              <a className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-gray-600 hover:opacity-75" href="#pablo">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M23.111 20.058l-4.977-4.977c.965-1.52 1.523-3.322 1.523-5.251 0-5.42-4.409-9.83-9.829-9.83-5.42 0-9.828 4.41-9.828 9.83s4.408 9.83 9.829 9.83c1.834 0 3.552-.505 5.022-1.383l5.021 5.021c2.144 2.141 5.384-1.096 3.239-3.24zm-20.064-10.228c0-3.739 3.043-6.782 6.782-6.782s6.782 3.042 6.782 6.782-3.043 6.782-6.782 6.782-6.782-3.043-6.782-6.782zm2.01-1.764c1.984-4.599 8.664-4.066 9.922.749-2.534-2.974-6.993-3.294-9.922-.749z"/></svg>
                              </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
      </>
    );
}