import Image from "next/image";

export default ({src, height, width, caption}) => {
    return (
        <>
            <div className="container mx-auto">
                <div className="grid grid-rows-2 grid-cols-1 gap-4">
                    <div className="m-4 p-4 row-span-1 col-span-1">
                        <Image
                            src={src}
                            height={height}
                            width={width}
                            alter={caption}
                        />
                    </div>
                    <div className="row-span-1 col-span-1">
                        <h4 className="text-gray-600 text-4xl px-20">{caption}</h4>
                    </div>

                </div>
            </div>
        </>
    );
}