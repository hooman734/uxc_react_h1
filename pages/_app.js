import "tailwindcss/tailwind.css";
import Navbar from "../components/Navbar"
import Content from "../components/Content";
import SamplePhoto from "../components/SamplePhoto";


function MyApp({ Component, pageProps }) {
  return (
      <>
          <div style={{background: "url('/home-background.jpeg') fixed center"}}>
              <Navbar/>
              <Content/>
          </div>
          <div className="bg-gray-100">
              <SamplePhoto src={'/home-background.jpeg'} width={'600'} height={'600'} caption={"Test Image"}></SamplePhoto>
              <SamplePhoto src={'/home-background.jpeg'} width={'400'} height={'700'} caption={"Test Image2"}></SamplePhoto>
          </div>

          <Component {...pageProps} />
      </>
  );
}

export default MyApp
