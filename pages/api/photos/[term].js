import { createApi } from "unsplash-js";


const api = createApi({
    accessKey: "S1Z6Bg5SJJPtnOAXXEslBt7hVm5bTONmxcCwBuKIKnA",
    //secretKey: "FfI2C6Sqf9SdrJFSdEvZdoexLCWqqN_yE9jxmKGx61g"
  });
  

export default (req, res) => {
  const randIndex = Math.floor(Math.random() * 10);
  const {
    query: { term },
  } = req;
  api.search
  .getPhotos({query: `${term}`, orientation: "landscape", count: 1})
  .then(result => res.status(200).json(result.response.results[randIndex].urls.full))
  .catch(() => {
    console.log("something went wrong!");
  });
}
  